data "aws_route53_zone" "mydomain_dns" {
  name = "marklouisdelapena.com"
}
data "aws_elastic_beanstalk_hosted_zone" "current" {}

resource "aws_route53_record" "elasticbeanstalk_DNS" {
  zone_id = "${data.aws_route53_zone.mydomain_dns.zone_id}"
  name    = "exam.${data.aws_route53_zone.mydomain_dns.name}"
  type    = "A"
  alias {
    name                   = "${aws_elastic_beanstalk_environment.ebsexam-env.cname}"
    zone_id                = "${data.aws_elastic_beanstalk_hosted_zone.current.id}"
    evaluate_target_health = true
  }
}