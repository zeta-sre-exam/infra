variable "aws_region" {
  default = "ap-southeast-1"
}

variable "service_role" {
  description = "Service Role"
  default = "AWS-Elasticbeanstalk-Service-Role"
}

variable "instance_role" {
  description = "Instance Role"
  default = "aws-elasticbeanstalk-ec2-role"
}

variable "vpc_id" {
  default = "vpc-b765a2d1"
}

variable "subnet_id" {
  default = "subnet-26ba616e"
}
