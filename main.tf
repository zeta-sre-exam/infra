resource "aws_elastic_beanstalk_environment" "ebsexam-env" {
  name                = "sinatra-env"
  application         = "sinatra-app"
  cname_prefix        = "sinatra-env"


  solution_stack_name = "64bit Amazon Linux 2018.03 v2.16.8 running Docker 19.03.13-ce"


  setting {
    namespace = "aws:ec2:vpc"
    name      = "VPCId"
    value     = "${var.vpc_id}"
  }
  setting {
    namespace = "aws:ec2:vpc"
    name      = "Subnets"
    value     = "${var.subnet_id}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${var.service_role}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${var.instance_role}"
  }
}

