resource "aws_s3_bucket" "exam-bucket" {
  bucket = "sreexam-bucket-versions"
  acl    = "private"

  tags = {
    Name        = "exam-bucket"
  }
}

